import decimal


def frange(x, y, jump):
    while x < y:
        yield float(x)
        x += decimal.Decimal(jump)


for i in frange(1, 100, 3.5):
    print(i)
