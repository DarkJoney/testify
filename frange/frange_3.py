def frange(start, stop=None, step=None): # noqa
    if stop is None:
        value = 0.0
        stop = start
        step = 1.0
    elif step is None:
        value = start
        stop = stop
        step = 1.0
    else:
        value = start
        stop = stop
        step = step
    while value < stop and step > 0 or value > stop and step < 0:
        yield value
        value += step


for i in frange(1, 100, 3.5):
    print(i)
