class frange: # noqa
    def __init__(self, start, stop=None, step=None):
        if stop is None:
            self.value = 0.0
            self.stop = start
            self.step = 1.0
        elif step is None:
            self.value = start
            self.stop = stop
            self.step = 1.0
        else:
            self.value = start
            self.stop = stop
            self.step = step

    def __iter__(self):
        return self

    def __next__(self):
        if self.value < self.stop and self.step > 0 or self.value > self.stop and self.step < 0:
            self.value += self.step
            return self.value - self.step
        else:
            raise StopIteration


for i in frange(1, 100, 3.5):
    print(i)
