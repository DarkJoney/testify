from django.contrib import admin

# Register your models here.
from testify.forms import AnswerInlineFormSet, QuestionForm, QuestionsInlineFormSet
from testify.models import Answer, Question, Test, TestResult, Topic


class AnswerInline(admin.TabularInline):
    model = Answer
    fields = ('text', 'is_correct')
    show_change_link = True
    formset = AnswerInlineFormSet
    extra = 1


class QuestionAdmin(admin.ModelAdmin):
    form = QuestionForm
    inlines = (AnswerInline,)


class QuestionsInline(admin.TabularInline):
    model = Question
    fields = ('text', 'order_number')
    show_change_link = True
    extra = 0
    formset = QuestionsInlineFormSet
    form = QuestionForm
    ordering = ('order_number',)


class TestAdmin(admin.ModelAdmin):
    inlines = (QuestionsInline,)


admin.site.register(Topic)
admin.site.register(Test, TestAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer)
admin.site.register(TestResult)
