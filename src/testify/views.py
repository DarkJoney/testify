from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from django.views.generic import ListView, DetailView

from accounts.models import User
from testify.forms import AnswerFormSet
from testify.models import Question, Test, TestResult
from decimal import Decimal


class QuestionView(View):

    def get(self, request, test_id, order_number):
        question = Question.objects.get(test__id=test_id, order_number=order_number)
        answers = question.answers.all()
        form_set = AnswerFormSet(queryset=answers)

        return render(
            request=request,
            template_name='question.html',
            context={
                'question': question,
                'form_set': form_set,
            }
        )

    def post(self, request, test_id, order_number):
        question = Question.objects.get(test__id=test_id, order_number=order_number)
        test = question.test
        answers = question.answers.all()
        form_set = AnswerFormSet(data=request.POST)
        possible_choices = len(form_set.forms)
        selected_choices = [
            # form.cleaned_data.get('is_selected', False)
            'is_selected' in form.changed_data
            for form in form_set.forms
        ]

        num_selected_choices = sum(selected_choices)

        if num_selected_choices == 0:
            messages.error(self.request, extra_tags='danger', message="ERROR: You should select at least 1 answer!")
            return redirect(reverse('tests:question', args=(test_id, order_number)))

        if num_selected_choices == possible_choices:
            messages.error(self.request, extra_tags='danger', message="ERROR: You can't select ALL answer!")
            return redirect(reverse('tests:question', args=(test_id, order_number)))

        correct_choices = sum(
            answer.is_correct == choice
            for answer, choice in zip(answers, selected_choices)
        )

        point = int(correct_choices == possible_choices)
        point_weighted = question.test.questions.count() - correct_choices

        if point_weighted < 0:
            point_weighted = 0

        current_test = request.session['current_test']
        current_test['num_correct_answers'] += point
        current_test['num_incorrect_answers'] += (1 - point)

        if order_number == question.test.questions.count():
            user = User.objects.get(username=request.user)
            current_rating = user.rating
            new_rating = current_rating + Decimal(point_weighted)
            current_test['points_achieved'] = new_rating
            user.rating = new_rating
            user.save()

        if order_number == question.test.questions_count:
            test_result = TestResult.objects.create(
                user=request.user,
                state=TestResult.STATE.FINISHED,
                test=test,
                num_correct_answers=current_test['num_correct_answers'],
                num_incorrect_answers=current_test['num_incorrect_answers'],
            )
            return render(
                request=request,
                template_name='finish.html',
                context={
                    'test_result': test_result,
                    'time_spent': test_result.write_date - test_result.create_date,
                    'test_result_score': test_result.num_correct_answers/test.questions_count
                }
            )
        else:
            return redirect(reverse('tests:question', args=(test_id, order_number+1)))


class TestView(DetailView):
    model = Test
    template_name = 'details.html'
    context_object_name = 'test'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        all_results = TestResult.objects.filter(test=self.kwargs['id'])
        context['all_results'] = all_results
        context['best_result'] = TestResult.best_result(test_id=self.kwargs['id'])
        context['last_run'] = TestResult.last_run(test_id=self.kwargs['id'])
        return context


class TestStartView(View):
    command = None

    def get(self, request, test_id):
        if 'current_test' not in request.session:
            context = {
                'test_id': test_id,
                'order_number': 1,
                'num_correct_answers': 0,
                'num_incorrect_answers': 0
            }
            request.session['current_test'] = context

        context = request.session['current_test']

        order_number = context['order_number']

        return redirect(reverse('tests:question', args=(test_id, order_number)))


class TestListView(ListView):
    model = Test
    template_name = 'list.html'
    context_object_name = 'tests'
