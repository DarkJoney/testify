from django.urls import path

from testify.views import QuestionView, TestListView, TestView, TestStartView

app_name = 'tests'

urlpatterns = [
    path('', TestListView.as_view(), name='list'),
    path('<int:test_id>/question/<int:order_number>', QuestionView.as_view(), name='question'),
    path('<int:id>/', TestView.as_view(), name='details'),
    path('<int:test_id>/start/', TestStartView.as_view(command='start'), name='start'),
    # path('<test_id>/next/', TestRunView.as_view(command='next'), name='next'),
    # path('<test_id>/finish/', TestRunView.as_view(command='finish'), name='finish'),
    # path('<test_id>/run/', TestRunnerView.as_view(), name='run'),
]
