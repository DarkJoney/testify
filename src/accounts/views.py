from django.contrib import messages

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
# Create your views here.
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, UpdateView, ListView

from accounts.forms import AccountCreateForm, AccountUpdateForm
from accounts.models import User


class AccountCreateView(CreateView):
    model = User
    template_name = 'registration.html'
    form_class = AccountCreateForm
    success_url = reverse_lazy('accounts:login')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.info(self.request, f"New user has just been created!") # noqa
        return result


class AccountLoginView(LoginView):
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('core:index')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.info(self.request, f'User {self.request.user} has been successfully logged in!')
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'

    def get(self, request, *args, **kwargs):
        result = super().get(request, *args, **kwargs)
        messages.info(self.request, f'User {self.request.user} has been logged out!')
        return result


class AccountUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'profile.html'
    form_class = AccountUpdateForm
    success_url = reverse_lazy('core:index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['all_results'] = self.get_object().test_results.all()
        return context

    def get_object(self, queryset=None):
        return self.request.user


class AccountPasswordUpdateView(LoginRequiredMixin, PasswordChangeView):
    model = User
    template_name = 'password.html'
    success_url = reverse_lazy('accounts:login')


class AccountListView(ListView):
    model = User
    template_name = 'user_list.html'
    context_object_name = 'users'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        users = User.objects.order_by('-rating')
        context['users'] = users
        return context


class LeaderboardListView(ListView):
    model = User
    template_name = 'leaderboards.html'
    context_object_name = 'users'
    queryset = User.objects.filter(rating__gt=0).order_by('-rating')
