from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    rating = models.DecimalField(default=0.0, decimal_places=2, max_digits=5, validators=[MinValueValidator(0),
                                                                                          MaxValueValidator(100)])
    image = models.ImageField(null=True, default='default.jpg', upload_to='pics/')

    def full_name(self):
        return f'{self.first_name} {self.last_name}'
