from django.urls import path

from accounts.views import (
    AccountCreateView,
    AccountLoginView,
    AccountLogoutView,
    AccountUpdateView,
    AccountListView,
    LeaderboardListView,
)

app_name = 'accounts'

urlpatterns = [
    path('register/', AccountCreateView.as_view(), name='register'),
    path('login/', AccountLoginView.as_view(), name='login'),
    path('logout/', AccountLogoutView.as_view(), name='logout'),
    path('profile/', AccountUpdateView.as_view(), name='profile'),
    path('leaderboard/', LeaderboardListView.as_view(), name='leaderboard'),
    path('', AccountListView.as_view(), name='users'),
]
